import React from 'react';
import {Container, Row, Col, Table} from 'reactstrap';
import './styles.css';
export default class AboutCont extends React.Component {
    render() {
        return (
            <Container className="About">
                <Row>
                    <Col xs="3"></Col>
                    <Col xs="6"><h2>Credits</h2></Col>
                    <Col xs="3"></Col>
                </Row>
                <Row >
                    <Col xs="3"></Col>
                    <Col xs="6"  className="tableAbout">
                        <Table hover>
                            <tr>
                                <td>navbar background</td>
                                <td><a href="https://www.freepik.com/free-vector/gold-diamond-pattern-on-marble-texture_1967209.htm">Designed
                                    by Kjpargeter</a></td>
                            </tr>
                            <tr>
                                <td>background</td>
                                <td><a href='https://www.freepik.com/free-vector/modern-geometric-background-with-connecting-dots_1126591.htm'>Designed
                                    by Balasoiu</a></td>
                            </tr>
                            <tr>
                                <td>Framework</td>
                                <td><a href='https://reactstrap.github.io/'>Reactstrap</a></td>
                            </tr>
                            <tr>
                                <td>Fake Data</td>
                                <td><a href='http://jsonplaceholder.typicode.com/'>jsonplaceholder</a></td>
                            </tr>

                        </Table>
                    </Col>
                    <Col xs="3"></Col>
                </Row>
            </Container>
        );
    }
}