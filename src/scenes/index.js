import Dashboard from './dashboard';
import Users from './users';
import About from './about';
var Scenes = {
    About,
    Dashboard,
    Users
}

export default Scenes;