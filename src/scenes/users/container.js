import React from 'react';
import {
    Container,
    Row,
    Col
} from 'reactstrap';
import Post from "./posts";
import UserDetail from "./userDetail";
export default class UsersCont extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            post: [],
            user: {},
            inputTitle: "",
            inputStory: "",
            modal: false,
            currentUser: {},
            userModal: {}
        };

    }

    componentWillMount() {
        fetch("https://jsonplaceholder.typicode.com/posts?userId="+this.props.userProfileId)
            .then(response => response.json())
            .then(json => {
                json.sort((a, b) => {
                    return b.id - a.id;
                });
                this.setState({ post: json });
            });
        fetch("https://jsonplaceholder.typicode.com/users/"+this.props.userProfileId)
            .then(response => response.json())
            .then(json => this.setState({ user: json }));
    }

    render() {
        return (
            <Container>
            <Row>
                <Col xs="4">
                    <UserDetail
                        user={this.state.user}
                    />
                </Col>
                <Col xs="8">
                    {this.state.post.map(post => {
                        var tusr = {};

                        return (
                            <Post
                                user={this.state.user}
                                key={post.id}
                                id={post.id}
                                title={post.title}
                                body={post.body}
                                toggleModalUser={()=>this.toggleModalUser(tusr.id)}
                            />
                        );
                    })}
                </Col>
            </Row>
            </Container>
        );
    }
}