import React from 'react';
import "./users.css";
import {Card, CardTitle,
    CardBody, CardHeader,
    CardFooter, CardText,
    Row, Col} from 'reactstrap';
const Post = (props) => {
    return (
        <Row  className="posts">
            <Col>
                <Card>
                    <CardHeader className="text-left">
                        {JSON.stringify(props.user.username)}
                    </CardHeader>
                    <CardBody key={props.id}>
                        <CardTitle>
                            {props.title}
                        </CardTitle>
                        <CardText className="text-left">
                            {props.body}
                        </CardText>
                    </CardBody>
                    <CardFooter className="text-right comment">

                    </CardFooter>
                </Card>
            </Col>
        </Row>
    )
};

export default Post
