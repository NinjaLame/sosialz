import React from 'react';
import "./users.css";
import {Card,CardBody, CardHeader,CardText,Row,Col, Table} from 'reactstrap';
const UserDetail = (props) => {
    return (
        <Row  className="posts sticky-top">
            <Col>
                <Card>
                    <CardHeader className="text-left" >
                        Hello! I'm {props.user.username}
                    </CardHeader>
                    <CardBody key={props.id}>
                        <CardText className="text-left">
                            <Table>
                                <tbody>
                                <tr>
                                    <td>Name</td>
                                    <td>{props.user.name}</td>
                                </tr>
                                <tr>
                                    <td>Email</td>
                                    <td>{props.user.email}</td>
                                </tr>
                                <tr>
                                    <td>Phone</td>
                                    <td>{props.user.phone}</td>
                                </tr>
                                <tr>
                                    <td>Website</td>
                                    <td>{props.user.website}</td>
                                </tr>

                                </tbody>
                            </Table>
                        </CardText>
                    </CardBody>
                </Card>
            </Col>
        </Row>
    )
};

export default UserDetail;
