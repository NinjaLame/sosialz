import React from "react";
import { Container, Row, Col } from "reactstrap";
import Post from "./post";
import FormPost from "./formPost";
import UserModal from "./userModal";
import "./dashboard.css";
import EditModal from './editModal';
import DeleteModal from './deleteModal';
export default class DashboardCont extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            post: [],
            user: [],
            inputTitle: "",
            inputStory: "",
            modal: false,
            editModal: false,
            deleteModal:false,
            currentUser:{},
            userModal:{}
        };

    }

    componentDidMount () {
        fetch("https://jsonplaceholder.typicode.com/posts")
            .then(response => response.json())
            .then(json => {
                json.sort((a, b) => {
                    return b.id - a.id;
                });
                this.setState({ post: json });
            });
        fetch("https://jsonplaceholder.typicode.com/users")
            .then(response => response.json())
            .then(json => this.setState({ user: json }));

    }
    componentWillMount() {
        var that = this;
        setTimeout(()=>{
            const user = this.props.currentUser;
            this.setState({currentUser:user});
        }, that.props.wait)
    }


    async uploadPost() {
        // POST adds a random id to the object sent
        fetch("https://jsonplaceholder.typicode.com/posts", {
            method: "POST",
            body: JSON.stringify({
                title: this.state.inputTitle,
                body: this.state.inputStory,
                userId: this.state.inputUser
            }),
            headers: {
                "Content-type": "application/json; charset=UTF-8"
            }
        })
            .then(response => response.json())
            .then(json => {
                json = [json, ...this.state.post];
                this.setState({ post: json });
            });
        this.state.inputTitle = "";
        this.state.inputStory = "";
    }
    async deletePost (id) {
        fetch('https://jsonplaceholder.typicode.com/posts/'+id, {
            method: 'DELETE'
        });
        console.log("hello");
        var posts = this.state.post;
        var filtered = posts.filter(function(value, index, arr){

            return value.id !== id;

        });
        this.setState({
            post:filtered,
            userModal:{},
            deleteModal:!this.state.deleteModal
        })
    }
    async editPost(id) {
        // POST adds a random id to the object sent
        fetch("https://jsonplaceholder.typicode.com/posts/"+id, {
            method: "PUT",
            body: JSON.stringify({
                id: id,
                title: this.state.inputTitle,
                body: this.state.inputStory,
            }),
            headers: {
                "Content-type": "application/json; charset=UTF-8"
            }
        })
            .then(response => response.json())
            .then(json => {
                var posts = this.state.post;
                var i;
                for (i= 0; i <posts.length; i++){
                    if(json.id === posts[i].id){
                        posts[i].title = json.title;
                        posts[i].body = json.body;
                        break;
                    }
                }
                console.log(posts);

                this.setState({
                    post: posts,
                    userModal:{},
                    editModal:!this.state.editModal
                });

            });
        this.state.inputTitle = "";
        this.state.inputStory = "";
    }

    changeTitle(e) {
        this.setState({ inputTitle: e.target.value });
    }
    changeStory(e) {
        this.setState({ inputStory: e.target.value });
    }
    changeUser(e){
        this.setState({inputUser:e.target.selectedIndex})
    }

    async toggleModalUser(id) {
        if (this.state.modal){
            await this.setState({
                userModal:{},
                modal:!this.state.modal
            });
        } else {

            const user = await fetch("https://jsonplaceholder.typicode.com/users/"+id)
                .then(respon => respon.json())
                .then(json => {
                    return json;
                });
            // console.log(user)
            await this.setState({
                userModal:user,
                modal:!this.state.modal
            });
        }
    }
    async toggleModalEdit(id){
        if (this.state.editModal){
            await this.setState({
                userModal:{},
                editModal:!this.state.editModal
            });
        } else {
            const user = await fetch("https://jsonplaceholder.typicode.com/posts/"+id)
                .then(respon => respon.json())
                .then(json => {
                    // console.log("https://jsonplaceholder.typicode.com/users/"+id);
                    return json;
                });
            // console.log(user)
            await this.setState({
                userModal:user,
                editModal:!this.state.editModal
            });
        }
    }
    async toggleModalDelete(id) {
        if (this.state.editModal){
            await this.setState({
                userModal:{},
                deleteModal:!this.state.deleteModal
            });
        } else {
            const user = await fetch("https://jsonplaceholder.typicode.com/posts/"+id)
                .then(respon => respon.json())
                .then(json => {
                    // console.log("https://jsonplaceholder.typicode.com/users/"+id);
                    return json;
                });
            // console.log(user)
            await this.setState({
                userModal:user,
                deleteModal:!this.state.deleteModal
            });
        }
    }
    openProfile(id){
        this.props.changeUserProfileId(id);
        this.props.click('userProfile');
    }
    render() {
        return (
            <Container className="dashboard">
                <UserModal
                    modal={this.state.modal}
                    toggle={()=>this.toggleModalUser(this.state.userModal.id)}
                    user={this.state.userModal}
                    openProfile={()=> this.openProfile(this.state.userModal.id)}
                />
                <EditModal
                    modal={this.state.editModal}
                    toggle={()=>this.toggleModalEdit(this.state.userModal.id)}
                    user={this.state.userModal}
                    updatePost={()=>this.editPost(this.state.userModal.id)}
                    inpTitle={e => this.changeTitle(e)}
                    inpStory={e => this.changeStory(e)}
                    valTitle={this.state.inputTitle}
                    valStory={this.state.inputStory}
                />
                <DeleteModal
                    modal={this.state.deleteModal}
                    toggle={()=>this.toggleModalDelete(this.state.userModal.id)}
                    deletePost={()=>this.deletePost(this.state.userModal.id)}
                />
                <Row>
                    <Col xs="2" />
                    <Col xs="8">
                        <FormPost
                            uplPost={() => this.uploadPost()}
                            inpTitle={e => this.changeTitle(e)}
                            inpStory={e => this.changeStory(e)}
                            inpUser={e => this.changeUser(e)}
                            valUser={this.state.inputUser}
                            valTitle={this.state.inputTitle}
                            valStory={this.state.inputStory}
                            users={this.state.user}

                        />
                        {this.state.post.map(post => {
                            var tusr = {};
                            for (var usr of this.state.user) {
                                if (usr.id === post.userId) {
                                    tusr = usr;
                                    break;
                                }
                            }
                            return (
                                <Post
                                    user={tusr}
                                    key={post.id}
                                    id={post.id}
                                    title={post.title}
                                    body={post.body}
                                    toggleModalUser={()=>this.toggleModalUser(tusr.id)}
                                    toggleModalEdit={()=>{
                                        this.setState({
                                            inputTitle:post.title,
                                            inputStory:post.body
                                        });
                                        return this.toggleModalEdit(post.id)}}
                                    toggleModalDelete={()=>this.toggleModalDelete(post.id)}
                                />
                            );
                        })}
                    </Col>
                    <Col xs="2" />
                </Row>
            </Container>
        );
    }
}
