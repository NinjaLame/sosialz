import React from 'react';
import { Modal, ModalHeader, ModalBody, Button,
    Card, CardHeader, CardFooter} from 'reactstrap';
const DeleteModal = (props) => {
    return (
        <div>
            <Modal isOpen={props.modal} toggle={props.toggle} className={props.className}>
                <ModalHeader toggle={props.toggle}>Delete Post</ModalHeader>
                <ModalBody>
                    <Card>
                        <CardHeader className="text-left">
                            Sure You Want to Delete this?
                        </CardHeader>
                        <CardFooter className="text-right">
                            <Button color="danger" onClick={props.deletePost}> Yes </Button>
                            <Button color="success" onClick={props.toggle}> Cancel </Button>
                        </CardFooter>
                    </Card>
                </ModalBody>
            </Modal>
        </div>
    )
};


export default DeleteModal;