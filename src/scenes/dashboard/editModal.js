import React from 'react';
import { Modal, ModalHeader, ModalBody, Button, InputGroup,
    Card, CardHeader, InputGroupAddon, CardFooter, Input} from 'reactstrap';
const EditModal = (props) => {
    return (
        <div>
            <Modal isOpen={props.modal} toggle={props.toggle} className={props.className}>
                <ModalHeader toggle={props.toggle}>Edit {props.user.username}'s Post</ModalHeader>
                <ModalBody>
                    <Card>
                        <CardHeader className="text-left">
                            <InputGroup>
                                <InputGroupAddon addonType="prepend" >Title </InputGroupAddon>
                                <Input onChange={props.inpTitle} value={props.valTitle} placeholder="What's the story title?" />
                            </InputGroup>
                            <br/>
                            <InputGroup >
                                <InputGroupAddon addonType="prepend" >Story </InputGroupAddon>
                                <Input onChange={props.inpStory} value={props.valStory} placeholder="Tell us Your story..." />
                            </InputGroup>
                        </CardHeader>
                        <CardFooter className="text-right">
                            <Button color="success" onClick={props.updatePost}> Save </Button>
                        </CardFooter>
                    </Card>
                </ModalBody>
            </Modal>
        </div>
    )
};


export default EditModal;