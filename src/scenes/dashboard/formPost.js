import React from 'react';
import "./dashboard.css"
import {Card, CardHeader, CardFooter,
    Row,Col, InputGroup, InputGroupAddon,
    Input, Button, CardTitle } from 'reactstrap';
const FormPost = (props) => {
    return (
        <Row  className="posts">
            <Col>
                <Card>
                    <CardHeader className="text-left">
                        <CardTitle> What's Your Story? </CardTitle>
                        <InputGroup>
                            <InputGroupAddon addonType="prepend" >Users </InputGroupAddon>
                            <Input type="select"
                                   onChange={props.inpUser}
                                   value={props.valUser}
                                   placeholder="Chose who'll post this.">
                                {props.users.map( user =>{
                                    return <option value={user.id} key={user.id}>{user.username}</option>
                                })}
                            </Input>
                        </InputGroup>
                        <br/>
                        <InputGroup>
                            <InputGroupAddon addonType="prepend" >Title </InputGroupAddon>
                            <Input onChange={props.inpTitle} value={props.valTitle} placeholder="What's the story title?" />
                        </InputGroup>
                        <br/>
                        <InputGroup >
                            <InputGroupAddon addonType="prepend" >Story </InputGroupAddon>
                            <Input onChange={props.inpStory} value={props.valStory} placeholder="Tell us Your story..." />
                        </InputGroup>
                    </CardHeader>
                    <CardFooter className="text-right">
                        <Button color="success" onClick={props.uplPost}> Post! </Button>
                    </CardFooter>
                </Card>
            </Col>
        </Row>
    )
};

export default FormPost
