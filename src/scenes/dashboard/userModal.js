import React from 'react';
import { Modal, ModalHeader, ModalBody,
    ModalFooter, Button, Table} from 'reactstrap';

const UserModal = (props) => {
    return (
        <div>
            {/*<Button color="danger" onClick={this.toggle}>{this.props.buttonLabel}</Button>*/}
            <Modal isOpen={props.modal} toggle={props.toggle} className={props.className}>
                <ModalHeader toggle={props.toggle}>{props.user.username}</ModalHeader>
                <ModalBody>
                    <Table>
                        <tbody>
                            <tr>
                                <td>Name</td>
                                <td>{props.user.name}</td>
                            </tr>
                            <tr>
                                <td>Email</td>
                                <td>{props.user.email}</td>
                            </tr>
                            <tr>
                                <td>Phone</td>
                                <td>{props.user.phone}</td>
                            </tr>
                            <tr>
                                <td>Website</td>
                                <td>{props.user.website}</td>
                            </tr>

                        </tbody>
                    </Table>
                </ModalBody>
                <ModalFooter>
                    <Button color="primary" onClick={props.openProfile}>See {props.user.username} Posts!!</Button>
                </ModalFooter>
            </Modal>
        </div>
    )
};


export default UserModal;