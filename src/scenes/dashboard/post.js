import React from 'react';
import "./dashboard.css"
import {Card, CardTitle,
    CardBody, CardHeader,
    CardFooter, CardText,
    Row, Col, Button, ButtonGroup} from 'reactstrap';
const Post = (props) => {
    return (
        <Row  className="posts">
            <Col>
                <Card>   
                <CardHeader className="text-left username" onClick={props.toggleModalUser}>
                    {JSON.stringify(props.user.username)} 
                </CardHeader>
                <CardBody key={props.id}>
                    <CardTitle>
                        {props.title}
                    </CardTitle>
                    <CardText className="text-left">
                        {props.body}
                    </CardText>
                </CardBody>
                <CardFooter className="text-right comment">
                    <ButtonGroup>
                        <Button color="info" onClick={props.toggleModalEdit}>Edit</Button>
                        <Button color="danger" onClick={props.toggleModalDelete}>Delete</Button>
                    </ButtonGroup>
                </CardFooter>
        </Card>
            </Col>
        </Row>
    )
};

export default Post
