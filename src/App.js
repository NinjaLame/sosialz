import React, { Component } from "react";
import "./App.css";
import Cont from "./container";
import Scenes from "./scenes";
import { CardFooter } from "reactstrap";
class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            scenes: "dashboard",
            userProfileId:'',
            currentUser:{}
        };
    }

    changeScenes(e) {
        this.setState({
            scenes: e
        });
    }

    changeUserProfileId(id){
        console.log(id);
        this.setState({
            userProfileId: id
        });
    }

    async getUser() {
        const respon = await fetch(
            "https://jsonplaceholder.typicode.com/users/1"
        );
        const json = await respon.json();
        return await json();
    }

    async componentWillMount() {
        const user = await fetch("https://jsonplaceholder.typicode.com/users/1")
            .then(respon => respon.json())
            .then(json => json);
        await this.setState({currentUser:user});
    }

    render() {
        var Na = Cont.Comp.NavbarComp;
        var Content;
        switch (this.state.scenes) {
            case "dashboard":
                Content = Scenes.Dashboard;
                break;
            case "about":
                Content = Scenes.About;
                break;
            case "userProfile":
                Content = Scenes.Users;
                break;
            default:
                Content = <div />;
                break;
        }
        var changeScenes = this.changeScenes;
        var changeUserProfileId = this.changeUserProfileId;
        return (
            <div className="App">
                <Na click={changeScenes.bind(this)} />
                <Content
                    changeUserProfileId={changeUserProfileId.bind(this)}
                    click={changeScenes.bind(this)}
                    className="cont"
                    currentUser={this.state.currentUser}
                    wait={3000}
                    userProfileId={this.state.userProfileId}
                />
                <CardFooter className="text-muted">Footer</CardFooter>
            </div>
        );
    }
}

export default App;
